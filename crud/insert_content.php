<?php
  include('configmongo.php');    
  $path_pic = null;
  if ($_POST['parent_id']=="null") {
    $content = null;
    $parentID = -1;
    $isContent = false;
  }else{
    $content = $_POST['con_content'];
    $parentID = $_POST['parent_id'];
    $isContent = true;
  }
  $data_id = $db->dragdrop_data->find()->limit(1)->sort(array('id'=>-1));
  foreach ($data_id as $value) {
    $id = $value['id'];
  }

  $order_count = $db->dragdrop_data->find(array('parentID'=>intval($parentID)))->limit(1)->sort(array('order'=>-1));
  foreach ($order_count as $count) {
    $order_id = $count['order'];
  }

  if ($isContent && !empty($_FILES['con_pic']['name'])) {
    $arraypic = explode(".",$_FILES['con_pic']['name']);
    $pointtype = count($arraypic);
    $filetype = strtolower($arraypic[$pointtype-1]);
    $randnum = rand(1000, 9999);
    $path_pic = "dragdrop-".$randnum.($id+1).".".$filetype;
    move_uploaded_file($_FILES["con_pic"]["tmp_name"],"../upload/".$path_pic);
    chmod('../upload/'.$path_pic, 0777);
  }

  $con = array(
    'title' => strval($_POST['con_title']),
    'id' => intval($id+1),
    'order' => intval($order_id+1),
    'parentID' => intval($parentID),
    'isContent'=> (bool)$isContent,
    'content'=> strval($content),
    'description' => strval($_POST['con_title']),
    'createBy'=> strval("kan"),
    'createDate'=> date("Y-m-d H:i:s"),
    'updateBy'=> strval("kan"),
    'updateDate'=> date("Y-m-d H:i:s"),
    'isEnable' => (bool)true,
    'path_pic' => strval($path_pic),
    );
  $con_insert = $db->dragdrop_data->insert($con);
  if ($con_insert) {
    header("location:../index.php");
  }else{
    echo $con_insert;
  }
?>