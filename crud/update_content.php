<?php
include('configmongo.php');
$path_pic = null;
$id_drag = $_POST['id'];
$result_content = $db->dragdrop_data->findOne(array('id'=> intval($id_drag)));
if (!empty($_FILES['con_pic']['name'])) {
	$arraypic = explode(".",$_FILES['con_pic']['name']);
    $pointtype = count($arraypic);
    $filetype = strtolower($arraypic[$pointtype-1]);
    $randnum = rand(1000, 9999);
    $path_pic = "dragdrop-".$randnum.".".$filetype;
    
  	if (move_uploaded_file($_FILES["con_pic"]["tmp_name"],"../upload/".$path_pic)) {
  		unlink('../upload/'.$result_content['path_pic']);
  	}
  	chmod('../upload/'.$path_pic, 0777);
}else{
  $path_pic = $result_content['path_pic'];
}
$db->dragdrop_data->update(array('id'=>intval($id_drag)),array(
	'$set' => array(
		'title' => strval($_POST['con_title']),
		'content' => strval($_POST['con_content']),
		'updateBy'=> strval("kan"),
    'updateDate'=> date("Y-m-d H:i:s"),
    'path_pic' => strval($path_pic),
    'description' => strval($_POST['con_description'])
		)
	));
header("location:../index.php");
?>