<!DOCTYPE html> 
<html>
<head>
  <meta charset="utf-8">
	<title>create title</title>
	<link href="css/jqueryUI/bootstrap.min.css" rel="stylesheet">
  <link href="css/jqueryUI/jquery-ui.min.css" rel="stylesheet">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui.min.js"></script>
  <script src="js/validate.js"></script>
</head>
<body>


  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        Create Title
      </a>
    </div>
  </div>
</nav>
<div class="container">
<form class="form-horizontal" method="POST" action="crud/insert_content.php" onsubmit="return validate_title()">

	<div class="form-group">
    <label for="con_title" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="con_title" name="con_title" placeholder="Title">
    </div>
  </div>

  <div class="col-sm-offset-5 col-sm-7">
    <button type="submit" id="con_save" name="con_save" class="btn btn-success">Save</button>
  </div>

  <input type="hidden" name="parent_id" value="null">

</form>
</div>
</body>
</html>