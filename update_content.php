<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=9">
	<title>update content</title>
	<link href="css/jqueryUI/bootstrap.min.css" rel="stylesheet">
  <link href="css/jqueryUI/jquery-ui.min.css" rel="stylesheet">
  <script src="js/jquery-1.11.0.min.js"></script>
  <script src="js/jquery-ui.min.js"></script>
  <script src="js/validate.js"></script>
</head>
<body>
<?php
  if (empty($_GET['id'])) {
    header('location:test.php');
  }
  include('crud/configmongo.php');
  $result_content = $db->dragdrop_data->findOne(array('id'=> intval($_GET['id'])));
  if (!$result_content['isEnable']) {
    header('location:test.php');
  }
?>
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        Update Content
      </a>
    </div>
  </div>
</nav>
<div class="container">
<form class="form-horizontal" method="POST" action="crud/update_content.php" enctype="multipart/form-data" onsubmit="return validate_title()">

	<div class="form-group">
    <label for="con_title" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="con_title" name="con_title" placeholder="Title" value="<?php echo $result_content['title'];?>">
    </div>
  </div>
  <?php
    if ($result_content['isContent']) {
  ?>
  <div class="form-group">
    <label for="con_description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-8">
      <textarea class="form-control" id="con_description" name="con_description" rows="3" placeholder="Description"><?php echo $result_content['description'];?></textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label for="con_content" class="col-sm-2 control-label">Content</label>
    <div class="col-sm-8">
      <textarea class="form-control" id="con_content" name="con_content" rows="3" placeholder="Content"><?php echo $result_content['content'];?></textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="con_pic" class="col-sm-2 control-label">Picture</label>
    <div class="col-sm-10">
      <input type="file" id="con_pic" name="con_pic" accept="image/*" onChange="validate_image(this);">
      <p>รูปภาพที่ใช้ได้ต้องเป็นนามสกุล .jpg .jpeg .png .gif</p>
    </div>
  </div>
  <?php
    if (!empty($result_content['path_pic'])) {

  ?>
  <div class="form-group">

    <div class="col-sm-offset-2 col-sm-10">
      <img src="upload/<?php echo $result_content['path_pic'];?>" width="663px" height="373px">
    </div>
  </div>
  <?php }}?>
  <div class="col-sm-offset-5 col-sm-7">
    <button type="submit" id="con_save" name="con_save" class="btn btn-success">Save</button>
  </div>

  <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">

</form>
</div>
</body>
</html>