<?php
	// init MongoDB
	$connectMongo 			= 		new MongoClient( 'mongodb://192.168.100.68:27017' );
	$DatabaseMongoDB		=		$connectMongo->selectDB("dragdrop");
	$collectionData			=		new MongoCollection($DatabaseMongoDB,"dragdrop_data");
	
	$arrData				=		array();
	$result_update			=		"";
	
	function recursive_sorting($arrData,$parentID){
	
		global	$collectionData;
	
		$size					=		count($arrData);
		$arrData['arrData']		=		$size-1;
		
		for( $i = 0 ; $i < $size ; $i++ ){
			$tmpData				=		$collectionData->findOne( array('id' =>  intval($arrData[$i]['data'])) );
			
			$tmpData['order']		=		($i+1);
			$tmpData['parentID']	=		intval($parentID);
			
			$collectionData->update(
				array('id' =>  intval($arrData[$i]['data'])),
				$tmpData
			);
			
			if(count($arrData[$i]) >= 2){
				recursive_sorting($arrData[$i],$arrData[$i]['data']);
			}
		}
	}
	
	if(is_array($_REQUEST['data'])){
	
		$size	=	count($_REQUEST['data']);
		$arrData['arrData']		=		$size;
		
		for( $i = 0 ; $i < $size ; $i++ ){
			$tmpData			=		$collectionData->findOne( array('id' =>  intval($_REQUEST['data'][$i]['data'])) );
			
			$tmpData['order']	=		($i+1);
			
			$result_update = $collectionData->update(
				array('id' =>  intval($_REQUEST['data'][$i]['data'])),
				$tmpData
			);
			
			if(count($_REQUEST['data'][$i]) >= 2){
				recursive_sorting($_REQUEST['data'][$i],$_REQUEST['data'][$i]['data']);
			}
		}
	}
	if ($result_update) {
		$arrData['is_success']	=	1;
	}
	
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($arrData) . ')';
	} else {
		echo json_encode($arrData);
	}
?>