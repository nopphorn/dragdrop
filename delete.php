<?php
	// init MongoDB
	$connectMongo 			= 		new MongoClient( 'mongodb://192.168.100.68:27017' );
	$DatabaseMongoDB		=		$connectMongo->selectDB("dragdrop");
	$collectionData			=		new MongoCollection($DatabaseMongoDB,"dragdrop_data");
	
	//$arrData				=		array();
	
	function recursive_deleting($parentID){
	
		global	$collectionData;
		
		$dataCursor				=		$collectionData->find( array( 'parentID' =>  $parentID ) );
		
		foreach($dataCursor as $tmpData){
			$collectionData->update(
				array( 'id'		=>  	$tmpData['id'] ),
				array( '$set'	=>		array( 'isEnable' => false ) )
			);
			recursive_deleting($tmpData['id']);
		}
	}
	
	if(isset($_REQUEST['id'])){
		$id		=		intval($_REQUEST['id']);
	}
	
	if($id>0){
		
		$tmpData			=		$collectionData->findOne( array( 'id' => $id ) );		
		$tmpParentID		=		$tmpData['parentID'];
		
		if($tmpData){
			$collectionData->update(
				array('id'		=>  	$id),
				array('$set'	=>		array( 'isEnable' => false ))
			);
			recursive_deleting($id);
		}
		
		$dataCursor				=		$collectionData->find( array( 'parentID' =>  $tmpParentID ) );
		$index					=		1;
		
		foreach($dataCursor as $tmpData){
			$collectionData->update(
				array( 'id'		=>  	$tmpData['id'] ),
				array( '$set'	=>		array( 'order' => $index ) )
			);
			$index++;
		}
		
	}
	
	/*$arrData['is_success']	=	1;
	
	if ($_REQUEST['callback'] != '') {
		echo $_REQUEST['callback'] . '(' . json_encode($arrData) . ')';
	} else {
		echo json_encode($arrData);
	}*/
	
	header( "location: " . $_SERVER['HTTP_REFERER'] );
	exit(0);
	
	
?>