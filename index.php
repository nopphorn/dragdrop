<!DOCTYPE html>
<html>
<head>	
	
	<title>List a Content</title>
	<link href="css/jqueryUI/bootstrap.min.css" rel="stylesheet">
	<link href="css/jqueryUI/jquery-ui.min.css" rel="stylesheet">
	<link href="css/pnotify.custom.min.css" rel="stylesheet">
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/pnotify.custom.min.js"></script>
</head>
<style type="text/css">
.btn-forlist {
	padding: 2px 5px;
}

.goggle_btn:hover { cursor: pointer; }
</style>
<body>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">List a Content</a>
		</div>
	</div>
</nav>

<?php
	// init MongoDB
	$connectMongo 			= 		new MongoClient( 'mongodb://192.168.100.68:27017' );
	$DatabaseMongoDB		=		$connectMongo->selectDB("dragdrop");
	$collectionData			=		new MongoCollection($DatabaseMongoDB,"dragdrop_data");
	
	function recursive_list($parent_id = -1,$level = 1){
		global	$collectionData;
		$tmpCursor			=		$collectionData->find( array( 'parentID' => $parent_id , 'isEnable' => true ) )->sort( array('order' => 1) );
		foreach( $tmpCursor as $tempData ){
			$tmpCursor			=		$collectionData->find( array('parentID' => $tempData['id'] , 'isEnable' => true ) )->sort( array('order' => 1) );
		?><li id="<?php echo $tempData['id']; ?>" class="level-<?php echo $level; ?> list-group-item" >
			<div class="goggle_btn" style="float: left;margin-top: 8px;"><p class="<?php if($tmpCursor->count() > 0) { ?> bullet_icon glyphicon glyphicon-chevron-up <?php }else{ ?> glyphicon glyphicon-minus <?php } ?>" ></p>&nbsp;<?php
			echo $tempData['title'];
			?></div><?php
			if($level<3){
				?>&nbsp;<a class="btn btn-forlist btn-success glyphicon glyphicon-plus" href="create_sub_content.php?parent_id=<?php echo $tempData['id']; ?>" role="button"></a><?php
			}?>&nbsp;<a class="btn btn-danger btn-forlist glyphicon glyphicon-minus" onclick="return confirm('ต้องการลบข้อมูลนี้จริงหรือไม่?');" href="delete.php?id=<?php echo $tempData['id']; ?>" role="button"></a>&nbsp;<a class="btn btn-forlist btn-warning glyphicon glyphicon-pencil" href="update_content.php?id=<?php echo $tempData['id']; ?>" role="button"></a>
			<a class="btn btn-forlist btn-info glyphicon glyphicon-search" href="read_content.php?id=<?php echo $tempData['id']; ?>" role="button"></a><?php
			if( $tmpCursor->count() > 0){ ?>
				<div class="panel panel-default" style="margin-top: 10px;"><ul class="list-group"><?php
					recursive_list($tempData['id'],$level+1);
				?></ul></div><?php
			}
		?>
		</li>
		<?php
		}
	}
	
	$cursorData				=		$collectionData->find( array( 'parentID' => -1 , 'isEnable' => true ) )->sort( array('order' => 1) );
?>

<div id="example3" class="panel panel-default" style="width: 50%;margin-left: 25%;">
	<center><a class="btn btn-success glyphicon glyphicon-plus" href="create_title.php" role="button" style="margin: 20px;">สร้างหัวข้อใหม่</a></center>
    <ul class="list-group">
		<?php
		$level = 1;
		foreach( $cursorData as $tmpData ){
			$tmpCursor			=		$collectionData->find( array('parentID' => $tmpData['id'], 'isEnable' => true ) )->sort( array('order' => 1) );
		?>
		<li id="<?php echo $tmpData['id']; ?>" class="level-<?php echo $level; ?> list-group-item" >
			<div class="goggle_btn" style="float: left;margin-top: 8px;"><p class="<?php if($tmpCursor->count() > 0) { ?> bullet_icon glyphicon glyphicon-chevron-up <?php }else{ ?> glyphicon glyphicon-minus <?php } ?>" ></p>&nbsp;<?php
			echo $tmpData['title'];
			?></div>&nbsp;<a class="btn btn-forlist btn-success glyphicon glyphicon-plus" href="create_content.php?parent_id=<?php echo $tmpData['id']; ?>" role="button"></a>&nbsp;<a class="btn btn-danger btn-forlist glyphicon glyphicon-minus" onclick="return confirm('ต้องการลบข้อมูลนี้จริงหรือไม่?');" href="delete.php?id=<?php echo $tmpData['id']; ?>" role="button"></a>&nbsp;<a class="btn btn-forlist btn-warning glyphicon glyphicon-pencil" href="update_content.php?id=<?php echo $tmpData['id']; ?>" role="button"></a>
			<?php
			if( $tmpCursor->count() > 0){
			?>
				<div class="panel panel-default" style="margin-top: 10px;"><ul class="list-group"><?php
				recursive_list($tmpData['id'],$level+1);
				?></ul></div><?php
			}
			?>
		</li>
		<?php
		}
		?>
    </ul>
	<center><button onclick="myFunction(1,null)" class="btn btn-default" id="submit_sort" autocomplete="off" data-loading-text="Loading..." style="margin: 20px;">ส่งข้อมูลการเรียงลำดับ</button></center>
</div>

<script>
	$.ajaxSetup({ cache: false });
	
	var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};
	
	$("#example3 ul").sortable({
		placeholder: "ui-state-highlight"
		/*update: function (event, ui) {
			$(this).sortable('refresh');
			var data = $(this).serializelist();
			console.log(data);
		}*/
	});
	
	$(document).ready(function() {
		//$('#hideDetailsDiv3').hide();
		$('.goggle_btn').click(function() {
		
			var obj		=	$(this).find(".bullet_icon");
			if(obj.hasClass("glyphicon-chevron-up")){
				obj.removeClass( "glyphicon-chevron-up" ).addClass( "glyphicon-chevron-down" );
			}else{
				obj.removeClass( "glyphicon-chevron-down" ).addClass( "glyphicon-chevron-up" );
			}
			
			$(this).nextAll(".panel").toggle(400);
		});
	});
	
	function recursive_order(arrOrder,id,level){
	
		var max 		= 		$('#' + id).find('.level-' + level ).length;
		var max_level	=		arrOrder.length;
		var tmpData		=		$('#' + id).find('.level-' + level );
		
		var	strPrefix	=		'&data';
		var	strAttr		=		'';

		for(var i=0 ; i < max_level ; i++ ){
			strPrefix	=		strPrefix + '[' + arrOrder[i]  + ']';
		}
		
		for( var i=0 ; i < max ; i++ ){
			
			arrOrder.push(i);
		
			strAttr				=		strAttr + strPrefix + '[' + i + '][data]=' + tmpData.attr('id');
			strAttr				=		strAttr + recursive_order(arrOrderTmp,tmpData.attr('id'),(level+1));
			tmpData				=		tmpData.next();
			
			arrOrder.pop();
		}
		
		return strAttr;
		
	}
	
	function myFunction(){

		var $btn = $('#submit_sort').button('loading');
	
		var max 		= 		$('.level-1').length;
		var tmpData		=		$('.level-1');
		
		var strAttr		=		'i=1';
		
		for(var i=0; i < max ; i++ ){
			arrOrderTmp			=		[i];
			strAttr				=		strAttr + '&data[' + i + '][data]=' + tmpData.attr('id');
			strAttr				=		strAttr + recursive_order(arrOrderTmp,tmpData.attr('id'),2);
			tmpData				=		tmpData.next();
		}
		
		var Url = 'http://spider.kapook.com/dragdrop/sorting.php?' + strAttr;
		$.ajax({
			url: Url,
			type: "GET",
			dataType: "json",
			success: function(datareturn) {
				if(datareturn.is_success!=1){
					console.log('Error!?!?!?');
					new PNotify({
						title: 'มีปัญหา',
						text: 'มีปัญหาในการจัดเรียงข้อมูล',
						type: 'error',
						addclass: "stack-bottomright",
						stack: stack_bottomright,
						mouse_reset: false
					});
					$btn.button('reset');
				}
				if(datareturn.is_success==1){
					console.log('Success.');
					new PNotify({
						title: 'สำเร็จ',
						text: 'ข้อมูลมีการจัดเรียงแล้ว',
						type: 'success',
						addclass: "stack-bottomright",
						stack: stack_bottomright,
						mouse_reset: false
					});
					$btn.button('reset');
				}
			}
		});
	}
	
</script>
